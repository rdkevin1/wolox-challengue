/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useEffect, useState } from 'react';
import { Provider as PaperProvider } from 'react-native-paper';
import { DefaultTheme, configureFonts } from 'react-native-paper'
import { NavigationContainer } from '@react-navigation/native';
import { MainStackNavigator } from './src/navigation/StackNavigator';
import Storage from './src/services/storage';
import { Provider } from 'mobx-react';
import store from './src/store/Store';

declare const global: { HermesInternal: null | {} };

const fontConfig: any = {
  default: {
    regular: {
      fontFamily: 'Nunito-Regular',
      fontWeight: 'normal',
    },
    light: {
      fontFamily: 'Nunito-Light',
      fontWeight: 'normal',
    },
    bold: {
      fontFamily: 'Nunito-Bold',
      fontWeight: 'normal',
    },
  },
};


const theme = {
  ...DefaultTheme,
  fonts: configureFonts(fontConfig),
  colors: {
    ...DefaultTheme.colors,
  }
};

const App = () => {
  const [isLogged, setIsLogged]: any = useState(null);
  useEffect(() => {
    validateIfLogged();
  }, []);

  const validateIfLogged = async () => {
    const userdata = await Storage.instance.getUserdataFromStorage();
    if (userdata != null) {
      setIsLogged(true);
    } else {
      setIsLogged(false);
    }
  }
  if (isLogged != null) {
    return (
      <>
        <Provider store={store}>
          <PaperProvider theme={theme}>
            <NavigationContainer>
              <MainStackNavigator isLogged={isLogged}></MainStackNavigator>
            </NavigationContainer>
          </PaperProvider>
        </Provider>
      </>
    );
  } else {
    return null;
  }
};

export default App;
