import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    layoutContainer: {
        paddingHorizontal: windowWidth * 0.07,
    },
    layoutLoginContainer: {
        flex: 1,
        paddingHorizontal: windowWidth * 0.07,
    },
    //Login styles
    loginLogoContainer: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    loginLogo: {
        width: windowWidth * 0.8,
        height: windowHeight * 0.3,
        marginTop: windowHeight * 0.02,
        marginBottom: windowHeight * 0.02,
        resizeMode: 'contain',
    },
    loginFormInputs: {
        marginBottom: windowHeight * 0.02,
    },
    errorInputs: {
        fontSize: 10,
        color: 'red'
    },
    pickerAge: {
        height: windowHeight * 0.05,
        width: windowWidth * 0.4
    },
    contentLoginButton: {
        height: windowHeight * 0.07,
    },
    loginButtonBorder: {
        borderRadius: windowWidth * 0.05,
        marginTop: windowHeight * 0.02
    },

    //Drawer component
    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: 20,
    },
    title: {
        fontSize: 16,
        marginTop: 3,
        fontWeight: 'bold',
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
    },
    row: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    paragraph: {
        fontWeight: 'bold',
        marginRight: 3,
    },
    drawerSection: {
        marginTop: 15,
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    preference: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
    //Icons styles
    libraryIcon: {
        width: 26,
        height: 26
    },
    backIcon: {
        width: 26,
        height: 26,
        marginLeft: windowWidth * 0.02
    },
    //App bar component styles
    appbarImageBackground: {
        width: '100%',
    },
    appbarContentContainer: {
        flexDirection: 'row',
        paddingTop: windowHeight * 0.02,
        paddingBottom: windowHeight * 0.05,
        paddingHorizontal: windowWidth * 0.02,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    appbarTitle: {
        color: 'white',
        fontSize: windowWidth * 0.06,
        fontWeight: 'bold'
    },

    //Home styles 
    spinnerContainer: {
        marginTop: windowHeight * 0.3,
    },
    homeContainer: {
        marginTop: windowHeight * 0.02,
        paddingHorizontal: windowWidth * 0.07,
    },
    bookList: {
        marginBottom: windowHeight * 0.17
    },
    //Book list component styles
    bookComponentContainer: {
        flexDirection: 'row',
        backgroundColor: 'white',
        borderRadius: windowWidth * 0.04,
        marginBottom: windowHeight * 0.02,
        paddingVertical: windowHeight * 0.03,
        paddingHorizontal: windowWidth * 0.1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    bookComponentImage: {
        width: windowWidth * 0.13,
        height: windowHeight * 0.1,
        resizeMode: 'cover'
    },
    bookComponentDescription: {
        flex: 1,
        marginLeft: windowWidth * 0.05
    },
    bookComponentTitle: {
        fontSize: windowWidth * 0.04,
        fontWeight: 'bold'
    },
    bookComponentSubtitle: {
        marginTop: 5,
        fontSize: windowWidth * 0.038,
    },
    //Book detail page styles
    bookDetailContainer: {
        backgroundColor: 'white',
        borderRadius: windowWidth * 0.02,
        marginTop: windowHeight * 0.02,
        marginBottom: windowHeight * 0.02,
        paddingTop: windowHeight * 0.02,
        paddingBottom: windowHeight * 0.04,
        paddingHorizontal: windowWidth * 0.06,

    },
    bookDetailRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    bookDetailRowImage: {
        width: windowWidth * 0.2,
        height: windowHeight * 0.18,
        marginRight: windowWidth * 0.06,
        resizeMode: 'contain'
    },
    bookDetailDescriptionContainer: {
        flex: 1
    },
    bookDetailTitle: {
        fontSize: windowWidth * 0.055,
        fontWeight: 'bold'
    },
    bookDetailAvailable: {
        color: '#B8D270',
        fontSize: windowWidth * 0.048
    },
    bookDetailParagraph: {
        lineHeight: windowWidth * 0.04,
        color: '#6C6C6C'
    },
    bookDetailButtonsContainer: {
        marginTop: windowHeight * 0.03,
        alignItems: 'center'
    },
    bookDetailButtonsHeight: {
        height: windowHeight * 0.06,
    },
    bookDetailWishButton: {
        width: windowWidth * 0.68,
        borderRadius: windowWidth * 0.05,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#71ADC0',
    },
    bookDetailRentButton: {
        width: windowWidth * 0.68,
        borderRadius: windowWidth * 0.05,
        marginTop: windowHeight * 0.01,
        backgroundColor: '#09B3E6'
    },
    commentsContainer: {
        backgroundColor: 'white',
        borderRadius: windowWidth * 0.02,
        marginBottom: windowHeight * 0.02,
        paddingHorizontal: windowWidth * 0.06,
        paddingBottom: windowHeight * 0.04
    },
    bookDetailCommentContainer: {
        flexDirection: 'row',
        marginTop: windowHeight * 0.04
    },
    bookDetailCommentTextContainer: {
        flex: 1,
        marginLeft: windowWidth * 0.04,
    },
    commentUserName: {
        fontSize: windowWidth * 0.04,
        fontWeight: 'bold'
    },
    commentText: {
        fontSize: windowWidth * 0.037,
        color: '#9A9A9A'
    },
    //Suggested list
    suggestedListContainer: {
        backgroundColor: 'white',
        borderRadius: windowWidth * 0.02,
        marginTop: windowHeight * 0.02,
        marginBottom: windowHeight * 0.02,
        paddingTop: windowHeight * 0.02,
        paddingBottom: windowHeight * 0.04,
        paddingHorizontal: windowWidth * 0.06,
    },
    suggestedListTitleContainer: {
        flex: 1,
        marginBottom: windowHeight * 0.02
    },
    suggestedListTitle: {
        fontSize: windowWidth * 0.04,
        fontWeight: 'bold'
    },
    suggestedListBookTitleContainer: {
        width: windowWidth * 0.2
    },
    suggestedListBookTitle: {
        textAlign: 'center',
    },

    contactPageContainer: {
        backgroundColor: 'white',
        borderRadius: windowWidth * 0.02,
        marginTop: windowHeight * 0.02,
        marginBottom: windowHeight * 0.02,
        paddingTop: windowHeight * 0.02,
        paddingBottom: windowHeight * 0.04,
        paddingHorizontal: windowWidth * 0.06,
    },
    contactPageBasicInfoContainer: {
        alignItems: 'center',
    },
    contactPageNameContainer: {
        marginTop: windowHeight * 0.02
    },
    contactPageName: {
        fontSize: windowWidth * 0.07,
        fontWeight: 'bold'
    },
    contactPageProfesionContainer: {
        width: windowWidth * 0.57,
        backgroundColor: 'black',
        paddingVertical: 10,
        borderRadius: 5,
        alignItems: 'center',
    },
    contactPageInfoContainer: {
        marginTop: windowHeight * 0.01,
        marginHorizontal: windowWidth * 0.08,
    },
    contactPageInfoTitle: {
        fontSize: windowWidth * 0.07,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    contactPageEmailText: {
        color: 'blue',
        textDecorationLine: 'underline',
        flex: 1
    },
    contactPageProfileContainer: {
        marginTop: windowHeight * 0.02,
    },
    contactPageProfileTitle: {
        fontSize: windowWidth * 0.07,
        fontWeight: 'bold',
        textAlign: 'center'
    },
});

export default styles;