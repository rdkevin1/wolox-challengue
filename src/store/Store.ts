import { observable, action, computed, makeAutoObservable } from "mobx";
import { Book, Userdata } from "../interfaces";

class Store {
    constructor() {
        makeAutoObservable(this);
    }

    @observable userdata: Userdata = { name: '', lastname: '', email: '', terms: false, age: '' }
    @observable allBooks: Array<Book> = [];
    @observable filteredBooks: Array<Book> = [];
    @observable suggestedBooks: Array<Book> = [];

    @action setUserdata = (value: Userdata) => {
        this.userdata = Object.assign(this.userdata, value);
    }

    @action setBooks = async (value): Promise<void> => {
        this.allBooks = value;
        this.filteredBooks = this.allBooks;
    }

    @action setFilteredBooks = async (filter: string): Promise<void> => {
        if (filter === '') {
            this.filteredBooks = this.allBooks;
        } else {
            this.filteredBooks = this.allBooks.filter(book => book.title.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
        }
    }

    @action setSuggestedBooks = async (title: string, filter: string): Promise<void> => {
        let books = this.allBooks.filter(book => book.genre === filter && book.title !== title);
        this.suggestedBooks = books;
    }

    @computed get getUserdata(): Userdata {
        return this.userdata;
    }

    @computed get getBooks(): Array<Book> {
        return this.allBooks;
    }

    @computed get getFilteredBooks(): Array<Book> {
        return this.filteredBooks;
    }

    @computed get getSuggestedBooks(): Array<Book> {
        return this.suggestedBooks;
    }
}

const store = new Store();
export default store;