import React from 'react';
import { Text, View, Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import styles from '../styles/styles';
import ImageLoader from './image_loader_component';

const BookListComponent = ({ book, navigation }: any) => {
    return (
        <TouchableOpacity onPress={() => navigation.navigate('book_detail', book)}>
            <View style={styles.bookComponentContainer}>
                <View>
                    {
                        book.image_url != null
                            ? <ImageLoader
                                style={styles.bookComponentImage}
                                source={{ uri: book.image_url }}
                            /> : <Image
                                style={styles.bookComponentImage}
                                source={require('../assets/images/no-image.jpg')}
                            />
                    }
                </View>
                <View style={styles.bookComponentDescription}>
                    <Text style={styles.bookComponentTitle}>{book.title}</Text>
                    <Text style={styles.bookComponentSubtitle}>{book.author}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
}

export default BookListComponent;
