import React from 'react';
import { IconButton } from 'react-native-paper';
import { TouchableOpacity, View, Image, ImageBackground, Text } from 'react-native';
import styles from '../styles/styles';

const AppbarComponent = ({ scene, previous, navigation }) => {
    const { options } = scene.descriptor;
    const title =
        options.headerTitle !== undefined
            ? options.headerTitle
            : options.title !== undefined
                ? options.title
                : scene.route.name;
    return (
        <ImageBackground source={require('../assets/images/bc_nav_bar.png')} style={styles.appbarImageBackground}>
            <View style={styles.appbarContentContainer}>
                {previous ? (
                    // <Appbar.BackAction
                    //     onPress={navigation.goBack}
                    // />
                    <TouchableOpacity
                        onPress={navigation.goBack}
                    >
                        <Image source={require('../assets/images/ic_back.png')} style={styles.backIcon} />
                    </TouchableOpacity>

                ) : (
                        <View>

                        </View>
                    )}
                <Text style={styles.appbarTitle}>
                    {title}
                </Text>
                <TouchableOpacity
                    onPress={() => {
                        navigation.openDrawer();
                    }}
                >
                    <IconButton icon="menu" color="white" size={30}></IconButton>
                </TouchableOpacity>
            </View>
        </ImageBackground>

    );
}

export default AppbarComponent;