import axios from 'axios';
class Request {
    static instance = new Request();

    private client = axios.create({
        baseURL: 'http://192.168.0.11:3001/'
    });

    async get(url: string): Promise<Object> {
        try {
            let req = await this.client.get(url);
            let data = req.data;
            return data;
        } catch (error) {
            console.log('Axios get method error', error);
            throw Error(error);
        }
    }

    async post(url: string, params: Object) {
        try {            
            let req = await this.client.post(url, params);
            let data = req.data;
            return data;
        } catch (error) {
            console.log('Axios post method error', error);
            throw Error(error);
        }
    }
}

export default Request;