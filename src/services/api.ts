import Request from './request';
import Storage from './storage';
import Toast from 'react-native-simple-toast';
import { Userdata } from '../interfaces';


class Api {
    static instance = new Api();
    login = async (data: Userdata): Promise<boolean> => {
        try {
            const url: string = 'sign_in';
            const response: Userdata = await Request.instance.post(url, data);
            Storage.instance.setUserdataInStorage(response);
            Toast.show('Login correcto', Toast.LONG);
            return true;
        } catch (error) {
            console.log(error);
            Toast.show('Ocurrio un error, intentelo de nuevo', Toast.LONG);
            return false;
        }
    }

    logout = async (navigation): Promise<void> => {
        try {
            await Storage.instance.removeUserdataFromStorage();
            Toast.show('Logout correcto', Toast.LONG);
            navigation.replace('login');
        } catch (error) {
            console.log(error);
        }
    }

    getBooks = async (): Promise<any> => {
        try {
            const url: string = 'books';
            const response = await Request.instance.get(url);
            return response;
        } catch (error) {
            console.log(error);
            Toast.show('Ocurrio un error, intentelo de nuevo', Toast.LONG);
            return [];
        }
    }
}

export default Api;