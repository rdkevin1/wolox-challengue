import AsyncStorage from '@react-native-async-storage/async-storage';
import { Userdata } from '../interfaces';
import store from '../store/Store';

class Storage {
    static instance = new Storage();

    setUserdataInStorage = async (userdata: Userdata): Promise<void> => {
        try {
            const jsonValue = JSON.stringify(userdata);
            await AsyncStorage.setItem('userdata', jsonValue);
        } catch (e) {
            console.log(e);
        }
    }

    getUserdataFromStorage = async () => {
        try {
            let jsonValue = await AsyncStorage.getItem('userdata');
            let parsedJson;
            if (jsonValue != null) {
                parsedJson = JSON.parse(jsonValue);
                store.setUserdata(parsedJson);
            } else {
                parsedJson = null;
            }
            return parsedJson;
        } catch (e) {
            console.log(e)
        }
    }

    removeUserdataFromStorage = async () => {
        try {
            await AsyncStorage.removeItem('userdata')
        } catch (e) {
            console.log(e);
        }
        console.log('Done.')
    }

}

export default Storage;