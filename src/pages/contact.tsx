import React from "react";
import { View, Text, Linking, TouchableOpacity } from 'react-native';
import { ScrollView } from "react-native-gesture-handler";
import { Avatar, Paragraph } from 'react-native-paper';
import styles from '../styles/styles';

const Contact = () => {
  return (
    <ScrollView showsVerticalScrollIndicator={false} style={styles.layoutContainer}>
      <View style={styles.contactPageContainer}>
        <View style={styles.contactPageBasicInfoContainer}>
          <Avatar.Image
            source={require('../assets/images/kevin_cv.jpg')}
            size={120}
          />
          <View style={styles.contactPageNameContainer}>
            <Text style={styles.contactPageName}>
              KEVIN BURBANO
            </Text>
          </View>
          <View style={styles.contactPageProfesionContainer}>
            <Text style={{ color: 'white' }}>SOFTWARE DEVELOPER</Text>
          </View>
        </View>
        <View style={styles.contactPageProfileContainer}>
          <Text style={styles.contactPageProfileTitle}>Perfil</Text>
          <Paragraph style={{textAlign: "justify"}}>
            Soy un desarrollador de software enfocado en el desarrollo de aplicaciones web y móviles, Soy un
            profesional que esta siempre en constante aprendizaje, sobre nuevas tecnologías y técnicas de
            programación, con alta capacidad para el trabajo en equipo y gran sentido de responsabilidad.
          </Paragraph>
        </View>
        <View style={styles.contactPageInfoContainer}>
          <Text style={styles.contactPageInfoTitle}>
            Contacto
          </Text>
          <View style={{ flexDirection: 'row' }}>
            <Text>E-mail: </Text>
            <TouchableOpacity onPress={() => Linking.openURL('mailto:burbarnokevin1997@gmail.com')}>
              <Text style={styles.contactPageEmailText}>burbanokevin1997@gmail.com</Text>
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text>Telefono: </Text>
            <TouchableOpacity onPress={() => Linking.openURL('tel:+57 3173785658')}>
              <Text style={styles.contactPageEmailText}>+57 317 378 56 58</Text>
            </TouchableOpacity>
          </View>
          <Text>Pais: Colombia</Text>
        </View>
      </View>
    </ScrollView>
  );
};

export default Contact;