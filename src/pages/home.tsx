import React, { useEffect, useState } from 'react';
import { Text, View, FlatList } from 'react-native';
import { ActivityIndicator, Colors, Searchbar } from 'react-native-paper';
import BookListComponent from '../components/book_list_component';
import styles from '../styles/styles';
import { inject, observer } from 'mobx-react';
import Api from '../services/api';

const HomePage = inject("store")(observer((props) => {
  const { getFilteredBooks, setBooks, setFilteredBooks } = props.store;
  const [isLoading, setIsLoading]: any = useState();
  const [searchQuery, setSearchQuery] = React.useState('');
  const onChangeSearch = (query) => {
    setSearchQuery(query);
    setFilteredBooks(query);
  };

  useEffect(() => {
    getAllBooks()
  }, [])

  const getAllBooks = async () => {
    setIsLoading(true);
    let books = await Api.instance.getBooks();
    setBooks(books);
    setIsLoading(false);
  }

  const bookList = () => {
    return (
      <FlatList data={getFilteredBooks}
        renderItem={({ item }) => <BookListComponent book={item} navigation={props.navigation}></BookListComponent>}
        keyExtractor={item => item.id.toString()}
        ListEmptyComponent={<Text>Sin resultados</Text>}
        showsVerticalScrollIndicator={false} style={styles.bookList} />
    );
  }

  return (
    <View>
      <View style={styles.homeContainer}>
        <Searchbar
          placeholder="Buscar"
          onChangeText={onChangeSearch}
          value={searchQuery}
        />
      </View>
      <View style={isLoading ? styles.spinnerContainer : styles.homeContainer}>
        {
          isLoading ?
            <ActivityIndicator animating={true} size="large" color={Colors.blue300} />
            : bookList()
        }
      </View>
    </View>
  );
}));

export default HomePage;

