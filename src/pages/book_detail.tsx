import React, { useEffect, useState, useRef } from 'react';
import { Text, View, Image, FlatList, TouchableOpacity } from 'react-native';
import { Paragraph, Button, Avatar } from 'react-native-paper';
import { inject, observer } from 'mobx-react';
import styles from '../styles/styles';
import { Book } from '../interfaces';
import Toast from 'react-native-simple-toast';
import { ActivityIndicator, Colors } from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';
import ImageLoader from '../components/image_loader_component';

const BookDetailPage = inject("store")(observer((props) => {

  const { getSuggestedBooks, setSuggestedBooks } = props.store;
  const [book, setBook]: any = useState(props.route.params);
  const [isLoading, setIsLoading]: any = useState(false);
  const scrollRef: any = useRef();

  useEffect(() => {
    setSuggestedBooks(book.title, book.genre);
  }, [])

  const setNewBookInDetailScreen = (book: Book): void => {
    setIsLoading(true);
    setBook(book);
    setTimeout(() => {
      setSuggestedBooks(book.title, book.genre);
      setIsLoading(false);
      scrollRef.current.scrollTo({
        y: 0,
        animated: true,
      });
    }, 500);
  }

  const SuggestedList = (book: Book) => {
    return (
      <TouchableOpacity onPress={() => setNewBookInDetailScreen(book)}>
        <View>
          {
            book.image_url != null
              ? <Image
                style={styles.bookDetailRowImage}
                source={{ uri: book.image_url }}
              /> : <Image
                style={styles.bookDetailRowImage}
                source={require('../assets/images/no-image.jpg')}
              />
          }
          <View style={styles.suggestedListBookTitleContainer}>
            <Text style={styles.suggestedListBookTitle}>{book.title}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  return (
    <ScrollView ref={scrollRef} style={styles.layoutContainer} showsVerticalScrollIndicator={false}>
      <View style={styles.bookDetailContainer}>
        <View style={styles.bookDetailRow}>
          {
            book.image_url != null
              ? <ImageLoader
                style={styles.bookDetailRowImage}
                source={{ uri: book.image_url }}
              /> : <Image
                style={styles.bookDetailRowImage}
                source={require('../assets/images/no-image.jpg')}
              />
          }
          <View style={styles.bookDetailDescriptionContainer}>
            <Text style={styles.bookDetailTitle}>{book.title}</Text>
            <Text style={styles.bookDetailAvailable}>Available</Text>
            <Paragraph style={styles.bookDetailParagraph}>
              {book.author} {"\n"}
              {book.year} {"\n"}
              {book.genre}
            </Paragraph>
          </View>
        </View>
        <View style={styles.bookDetailButtonsContainer}>
          <Button
            mode="contained"
            onPress={() => Toast.show('Añadido a lista de deseos', Toast.LONG)}
            labelStyle={{ color: '#1DACD4' }} contentStyle={styles.bookDetailButtonsHeight}
            style={styles.bookDetailWishButton}
          >
            ADD TO WISHLIST
          </Button>
          <Button
            mode="contained"
            onPress={() => Toast.show('Rentado', Toast.LONG)}
            contentStyle={styles.bookDetailButtonsHeight}
            style={styles.bookDetailRentButton}
          >
            RENT
          </Button>
        </View>
      </View>
      <View style={styles.commentsContainer}>
        <View>
          <View style={styles.bookDetailCommentContainer}>
            <Avatar.Image
              source={require('../assets/images/img_user.png')}
              size={65}
            />
            <View style={styles.bookDetailCommentTextContainer}>
              <Text style={styles.commentUserName}>
                John Applessed
              </Text>
              <Paragraph style={styles.commentText}>
                It was great to see you again
                earlier. Let´s definitely get that
                coffe it was great to see you again
                earlier.
              </Paragraph>
            </View>
          </View>
          <View style={styles.bookDetailCommentContainer}>
            <Avatar.Image
              source={require('../assets/images/img_user2.png')}
              size={65}
            />
            <View style={styles.bookDetailCommentTextContainer}>
              <Text style={styles.commentUserName}>
                Susan Collins
              </Text>
              <Paragraph style={styles.commentText}>
                Let´s definitely get that coffe it was
                great to see you again earlier.
              </Paragraph>
            </View>
          </View>
          <View>
            <Button mode='text' color='#8FC8E5' onPress={() => Toast.show('View all button', Toast.LONG)}>
              View all
            </Button>
          </View>
        </View>
      </View>
      <View style={styles.suggestedListContainer}>
        <View style={styles.suggestedListTitleContainer}>
          <Text style={styles.suggestedListTitle}>Libros sugeridos</Text>
        </View>
        {
          isLoading ?
            <ActivityIndicator animating={true} size="large" color={Colors.blue300} />
            : <FlatList data={getSuggestedBooks}
              renderItem={({ item }) => SuggestedList(item)}
              keyExtractor={item => item.id.toString()}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              ListEmptyComponent={<Text>No hay sugerencias para este genero</Text>}
            />
        }
      </View>
    </ScrollView>
  );
}));

export default BookDetailPage;
