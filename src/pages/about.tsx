import React from "react";
import { View, Text } from "react-native";
import { Avatar, Paragraph } from 'react-native-paper';
import { ScrollView } from "react-native-gesture-handler";
import styles from "../styles/styles";

const About = () => {
  return (
    <ScrollView showsVerticalScrollIndicator={false} style={styles.layoutContainer}>
      <View style={styles.contactPageContainer}>
        <View style={styles.contactPageBasicInfoContainer}>
          <Avatar.Image
            source={require('../assets/images/logo2.png')}
            size={160}
          />
        </View>
        <View style={styles.contactPageProfileContainer}>
          <Text style={styles.contactPageProfileTitle}>Quienes somos</Text>
          <Paragraph style={{ textAlign: "justify" }}>
            Somos una empresa tecnológica especializada en el desarrollo end-to-end de productos de alto impacto,
            brindando soluciones tecnológicas a start-ups y empresas que buscan
            innovar y necesitan apoyo en el desarrollo de sus ideas.
            Wolox se dedica a transformar industrias a través de la tecnología.
            Con oficinas en los EE. UU. Y América Latina, trabajamos junto a nuestros 
            clientes a lo largo de su proceso de innovación para crear soluciones de alto impacto.
          </Paragraph>
        </View>
      </View>
    </ScrollView>
  );
};


export default About;