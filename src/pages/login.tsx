import React, { useState } from 'react'
import { View, Text, Image } from 'react-native';
import { Button, TextInput, Checkbox } from 'react-native-paper';
import { Picker } from '@react-native-picker/picker';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Formik } from 'formik';
import * as yup from 'yup';
import styles from '../styles/styles';
import { ScrollView } from 'react-native-gesture-handler';
import { Userdata } from '../interfaces';
import Api from '../services/api';


const loginValidationSchema = yup.object().shape({
    name: yup
        .string()
        .required('Nombre requerido'),
    lastname: yup
        .string()
        .required('Apellido requerido'),
    email: yup
        .string()
        .email('Ingresa un E-mail valido')
        .required('E-mail requerido'),
    terms: yup
        .boolean()
        .oneOf([true], "Debes aceptar terminos y condiciones"),
    age: yup
        .string()
        .required('Edad es requerido')
    // password: yup
    //   .string()
    //   .min(8, ({ min }) => `Password must be at least ${min} characters`)
    //   .required('Password is required'),
});



const generateAgeListItems = (): Array<Element> => {
    let items: Array<Element> = [];
    for (let i = 15; i <= 80; i++) {
        items.push(<Picker.Item key={i} label={i.toString() + ' Años'} value={i} />);
    }
    return items;
}


const LoginPage = ({ navigation }: any) => {
    const [isLoading, setIsLoading] = useState(false);
    const login = async (navigation: any, data: Userdata): Promise<void> => {
        setIsLoading(true);
        const response = await Api.instance.login(data);
        if (response) {
            navigation.replace('home');
        } else {
            setIsLoading(false);
        }
    }
    return (
        <View style={styles.layoutLoginContainer}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.loginLogoContainer}>
                    <Image
                        style={styles.loginLogo}
                        source={require('../assets/images/logo.png')}
                    />
                </View>
                <View>
                    <Formik
                        validationSchema={loginValidationSchema}
                        initialValues={{ name: '', lastname: '', email: '', terms: false, age: '' }}
                        onSubmit={values => login(navigation, values)}
                    >
                        {({ handleChange, handleBlur, handleSubmit, setFieldValue, values, errors, isValid }) => (
                            <View>
                                <TextInput
                                    label="Nombre"
                                    mode="outlined"
                                    left={<TextInput.Icon color='darkgrey' name={() => <Icon name={'user'} size={20} color='grey' />} />}
                                    theme={{ colors: { primary: 'darkgrey' } }}
                                    value={values.name}
                                    onChangeText={handleChange('name')}
                                    style={styles.loginFormInputs} />
                                {errors.name &&
                                    <Text style={styles.errorInputs}>{errors.name}</Text>
                                }

                                <TextInput
                                    label="Apellido"
                                    mode="outlined"
                                    left={<TextInput.Icon name={() => <Icon name={'user'} size={20} color='grey' />} />}
                                    theme={{ colors: { primary: 'darkgrey' } }}
                                    value={values.lastname}
                                    onChangeText={handleChange('lastname')}
                                    style={styles.loginFormInputs} />
                                {errors.lastname &&
                                    <Text style={styles.errorInputs}>{errors.lastname}</Text>
                                }

                                <TextInput
                                    label="Email"
                                    mode="outlined"
                                    left={<TextInput.Icon name={() => <Icon name={'envelope'} size={20} color='grey' />} />}
                                    theme={{ colors: { primary: 'darkgrey' } }}
                                    keyboardType="email-address"
                                    value={values.email}
                                    onChangeText={handleChange('email')}
                                    style={styles.loginFormInputs} />
                                {errors.email &&
                                    <Text style={styles.errorInputs}>{errors.email}</Text>
                                }
                                <Text>Edad</Text>
                                <Picker
                                    selectedValue={values.age}
                                    mode="dialog"
                                    style={styles.pickerAge}
                                    onValueChange={(itemValue, itemIndex) => setFieldValue('age', itemValue)}
                                >
                                    {generateAgeListItems()}
                                </Picker>
                                {errors.age &&
                                    <Text style={styles.errorInputs}>{errors.age}</Text>
                                }

                                <Checkbox.Item
                                    label="Terminos & condiciones"
                                    labelStyle={{ color: 'black' }}
                                    status={values.terms ? 'checked' : 'unchecked'}
                                    onPress={() => {
                                        setFieldValue('terms', !values.terms);
                                    }}
                                />
                                {errors.terms &&
                                    <Text style={styles.errorInputs}>{errors.terms}</Text>
                                }
                                <View>
                                    <Button loading={isLoading} mode="contained" color='#ADC80A' labelStyle={{ color: 'white' }} onPress={handleSubmit} disabled={!isValid || isLoading} contentStyle={styles.contentLoginButton} style={styles.loginButtonBorder}>
                                        {isLoading ? '' : 'Login'}
                                    </Button>
                                </View>
                            </View>
                        )}
                    </Formik>
                </View>
            </ScrollView>
        </View>
    );
}

export default LoginPage;