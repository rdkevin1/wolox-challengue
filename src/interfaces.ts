export interface Userdata {
    name: string,
    lastname: string,
    email: string,
    terms: boolean,
    age: string
}

export interface Book {
    author: string,
    title: string,
    genre: string,
    publisher: string,
    year: string,
    image_url: string | null
}