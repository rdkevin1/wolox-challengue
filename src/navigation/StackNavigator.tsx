import React from "react";
import { CardStyleInterpolators, createStackNavigator } from "@react-navigation/stack";
import AppbarComponent from "../components/appbar_component";
import DrawerNavigator from "./DrawerNavigator";
import LoginPage from "../pages/login";
import HomePage from "../pages/home";
import BookDetailPage from "../pages/book_detail";
import ContactPage from "../pages/contact";
import About from "../pages/about";

const Stack = createStackNavigator();

const MainStackNavigator = ({isLogged}) => {    
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}
    >
      {isLogged ? (
        <>
          <Stack.Screen name="home" component={DrawerNavigator} options={{ headerShown: false }} />
          <Stack.Screen name="login" component={LoginPage} options={{ headerShown: false }} />
        </>
      ) : (
          <>
            <Stack.Screen name="login" component={LoginPage} options={{ headerShown: false }} />
            <Stack.Screen name="home" component={DrawerNavigator} options={{ headerShown: false }} />
          </>
        )}
    </Stack.Navigator>
  );
};

const HomeStackNavigator = () => {
  return (
    <Stack.Navigator
      headerMode='screen'
      screenOptions={{
        header: ({ scene, previous, navigation }) => (
          <AppbarComponent scene={scene} previous={previous} navigation={navigation}></AppbarComponent>
        ),        
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
      }}
    >
      <Stack.Screen name="home" component={HomePage} options={{ headerTitle: 'LIBRARY'}} />
      <Stack.Screen name="book_detail" component={BookDetailPage} options={{ headerTitle: 'BOOK DETAIL'}} />
    </Stack.Navigator>
  );
};

const ContactStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        header: ({ scene, previous, navigation }) => (
          <AppbarComponent scene={scene} previous={previous} navigation={navigation}></AppbarComponent>
        ),
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
      }}
    >
      <Stack.Screen name="contact" component={ContactPage} options={{ headerTitle: 'CONTACTO'}}/>
    </Stack.Navigator>
  );
};

const AboutStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        header: ({ scene, previous, navigation }) => (
          <AppbarComponent scene={scene} previous={previous} navigation={navigation}></AppbarComponent>
        ),
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
      }}
    >
      <Stack.Screen name="about" component={About} options={{ headerTitle: 'ABOUT'}}/>
    </Stack.Navigator>
  );
};

export { MainStackNavigator, HomeStackNavigator, ContactStackNavigator, AboutStackNavigator };