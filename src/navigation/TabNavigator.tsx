import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { ContactStackNavigator, HomeStackNavigator } from './StackNavigator';
import { Image } from 'react-native';
import { IconButton } from 'react-native-paper';
import styles from '../styles/styles';

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === 'Library') {
            iconName = focused ? require('../assets/images/ic_library_active.png') : require('../assets/images/ic_library.png');
            return <Image source={iconName} style={styles.libraryIcon}  />;
          } else if (route.name === 'Contact') {
            iconName = focused ? 'account-arrow-right' : 'account-arrow-right-outline';
            // You can return any component that you like here!
            return <IconButton icon={iconName} size={size} color={color} />;
          }


        },
      })}
      tabBarOptions={{
        activeTintColor: '#6DC3E2',
        inactiveTintColor: 'gray',
        tabStyle: {paddingVertical: 5}
      }}
    >
      <Tab.Screen name="Library" component={HomeStackNavigator} />
      <Tab.Screen name="Contact" component={ContactStackNavigator} />      
    </Tab.Navigator>
  );
}

export default BottomTabNavigator