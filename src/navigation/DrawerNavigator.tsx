import React from "react";
import { createDrawerNavigator } from '@react-navigation/drawer';
import TabNavigator from "./TabNavigator";
import { AboutStackNavigator } from "./StackNavigator";
import { DrawerComponent } from "../components/drawer_component";

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator drawerContent={props => <DrawerComponent {...props} />}>
      <Drawer.Screen name="Home" component={TabNavigator} />
      <Drawer.Screen name="About" component={AboutStackNavigator} />      
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;